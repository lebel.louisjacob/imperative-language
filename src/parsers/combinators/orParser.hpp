#ifndef COMBINATOR_OR_PARSER_HPP
#define COMBINATOR_OR_PARSER_HPP

#include <parser.hpp>

#include <type_traits>
#include <cstdint>

namespace imp
{
    template <typename... Parsers>
    struct OrParser final : public Parser<std::common_type_t<typename Parsers::Ast...>>
    {
        OrParser(Parsers const&... parsers):
            _parsers { parsers... }
        {}

        virtual typename OrParser::PartialOutput try_parse(typename OrParser::Input const& input) const& final override
        {
            return try_parse_recursive<0>(input);
        }

    private:
        std::tuple<Parsers const&...> _parsers;

        template <uint64_t index>
        typename OrParser::PartialOutput try_parse_recursive(typename OrParser::Input const& input) const&
        {
            typename OrParser::PartialOutput&& partial_output { std::get<index>(_parsers).try_parse(input) };

            if constexpr (index + 1 < sizeof...(Parsers))
            {
                if (partial_output.isLeft)
                {
                    return try_parse_recursive<index + 1>(input);
                }
            }
            return std::move(partial_output);
        }
    };
}

#endif
