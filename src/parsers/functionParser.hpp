#ifndef FUNCTION_PARSER_HPP
#define FUNCTION_PARSER_HPP

#include <parsers/statementParser.hpp>
#include <parsers/tokens/nameParser.hpp>
#include <parsers/tokens/skipParser.hpp>

#include <parser.hpp>

#include <ast/functionAst.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    struct FunctionParser final : public Parser<std::unique_ptr<FunctionAst>>
    {
        inline static SkipParser const open_parenthesis_parser { "(" };
        inline static SkipParser const closed_parenthesis_parser { ")" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& name_output { name_parser.try_parse(input) };
            if (name_output.isLeft) return PartialOutput::leftOf(name_output.leftValue);

            auto const&& open_parenthesis_ouput { open_parenthesis_parser.try_parse({ name_output.rightValue.remainder }) };
            if (open_parenthesis_ouput.isLeft) return PartialOutput::leftOf(open_parenthesis_ouput.leftValue);

            auto&& parameter_name_output { name_parser.try_parse({ open_parenthesis_ouput.rightValue.remainder }) };
            if (parameter_name_output.isLeft) return PartialOutput::leftOf(parameter_name_output.leftValue);

            auto const&& closed_parenthesis_ouput { closed_parenthesis_parser.try_parse({ parameter_name_output.rightValue.remainder }) };
            if (closed_parenthesis_ouput.isLeft) return PartialOutput::leftOf(closed_parenthesis_ouput.leftValue);

            auto&& statement_output { statement_parser.try_parse({ closed_parenthesis_ouput.rightValue.remainder }) };
            if (statement_output.isLeft) return PartialOutput::leftOf(statement_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: statement_output.rightValue.remainder,
                ast: std::make_unique<FunctionAst>(
                    std::move(name_output.rightValue.ast),
                    std::move(parameter_name_output.rightValue.ast),
                    std::move(statement_output.rightValue.ast)),
            });
        }
    } const function_parser;
}

#endif
