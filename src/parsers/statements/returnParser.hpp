#ifndef RETURN_PARSER_HPP
#define RETURN_PARSER_HPP

#include <parsers/expressionParser.hpp>
#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/statementAst.hpp>
#include <ast/statements/returnAst.hpp>

#include <parser.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    struct ReturnParser final : public Parser<std::unique_ptr<StatementAst>>
    {
        inline static SkipParser const return_token_parser { "return" };
        inline static ThenParser const statement_parser { return_token_parser, expression_parser };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& partial_output { statement_parser.try_parse(input) };
            if (partial_output.isLeft) return PartialOutput::leftOf(partial_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: partial_output.rightValue.remainder,
                ast: std::make_unique<ReturnAst>(std::move(partial_output.rightValue.ast)),
            });
        }
    } const return_parser;
}

#endif
