#ifndef WHILE_PARSER_HPP
#define WHILE_PARSER_HPP

#include <parsers/expressionParser.hpp>
#include <parsers/tokens/skipParser.hpp>
#include <parsers/combinators/thenParser.hpp>
#include <ast/statementAst.hpp>
#include <ast/statements/whileAst.hpp>

#include <parser.hpp>

#include <neither/either.hpp>
#include <memory>

namespace imp
{
    extern Parser<std::unique_ptr<StatementAst>> const& statement_parser;

    struct WhileParser final : public Parser<std::unique_ptr<StatementAst>>
    {
        inline static SkipParser const while_token { "while" };

        virtual PartialOutput try_parse(Input const& input) const& final override
        {
            auto&& while_token_output { while_token.try_parse(input) };
            if (while_token_output.isLeft) return PartialOutput::leftOf(while_token_output.leftValue);

            auto&& condition_output { expression_parser.try_parse({ while_token_output.rightValue.remainder }) };
            if (condition_output.isLeft) return PartialOutput::leftOf(condition_output.leftValue);

            auto&& statement_output { statement_parser.try_parse({ condition_output.rightValue.remainder }) };
            if (statement_output.isLeft) return PartialOutput::leftOf(statement_output.leftValue);

            return PartialOutput::rightOf(Output
            {
                remainder: statement_output.rightValue.remainder,
                ast: std::make_unique<WhileAst>(
                    std::move(condition_output.rightValue.ast),
                    std::move(statement_output.rightValue.ast)),
            });
        }
    } const while_parser;
}

#endif
