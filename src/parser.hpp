#ifndef PARSER_HPP
#define PARSER_HPP

#include <ast.hpp>
#include <visitor.hpp>

#include <utils/storable.hpp>
#include <neither/either.hpp>
#include <tuple>
#include <string_view>
#include <string>
#include <memory>

namespace imp
{
    struct Source
    {
        Source(std::string_view const& source):
            _source { source }
        {}

        bool not_empty() const&
        {
            return _source.size() > 0;
        }

        bool starts_with(std::string_view const& string) const&
        {
            return _source.rfind(string, 0) == 0;
        }

        template <typename Condition>
        std::string_view::size_type find_last_index_where(Condition test) const&
        {
            if (_source.size() == 0)
            {
                return 0;
            }

            std::string_view::size_type index { 0 };

            while (_source.size() > index && test(_source.at(index)))
            {
                ++index;
            }

            return index;
        }

        std::string_view segment(std::string_view::size_type const& size) const&
        {
            return _source.substr(0, size);
        }

        Source advance(std::string_view::size_type const& size) const&
        {
            return Source { _source.substr(size) };
        }

    private:
        std::string_view _source;
    };

    struct ParserInput
    {
        Source source;
    };

    template<typename _Ast>
    struct Parser
    {
        using Ast = _Ast;

        using Input = ParserInput;

        struct Output
        {
            Source remainder;
            [[no_unique_address]] Storable<Ast> ast;
        };

        using PartialOutput = neither::Either<std::string, Output>;

        virtual PartialOutput try_parse(Input const& input) const& = 0;
    };
}

#endif
