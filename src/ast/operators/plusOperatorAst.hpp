#ifndef PLUS_OPERATOR_AST_HPP
#define PLUS_OPERATOR_AST_HPP

#include <ast/operatorAst.hpp>

namespace imp
{
    struct PlusOperatorAst final : public AstImpl<PlusOperatorAst, OperatorAst>
    {};
}

#endif
