#ifndef ROOT_AST_HPP
#define ROOT_AST_HPP

#include <ast/functionAst.hpp>

#include <ast.hpp>

#include <vector>
#include <memory>

namespace imp
{
    struct RootAst final : public AstImpl<RootAst>
    {
        RootAst(std::vector<std::unique_ptr<FunctionAst const>>&& functions):
            functions { std::move(functions) }
        {}

        std::vector<std::unique_ptr<FunctionAst const>> functions;
    };
}

#endif
