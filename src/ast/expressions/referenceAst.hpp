#ifndef REFERENCE_AST_HPP
#define REFERENCE_AST_HPP

#include <ast/expressionAst.hpp>

#include <string_view>

namespace imp
{
    struct ReferenceAst final : public AstImpl<ReferenceAst, ExpressionAst>
    {
        ReferenceAst(std::string_view&& name):
            name { std::move(name) }
        {}

        std::string_view name;
    };
}

#endif
