#ifndef CALL_AST_HPP
#define CALL_AST_HPP

#include <ast/expressionAst.hpp>

#include <string_view>
#include <memory>

namespace imp
{
    struct CallAst final : public AstImpl<CallAst, ExpressionAst>
    {
        CallAst(std::string_view&& name, std::unique_ptr<ExpressionAst const>&& argument):
            name { std::move(name) },
            argument { std::move(argument) }
        {}

        std::string_view name;
        std::unique_ptr<ExpressionAst const> argument;
    };
}

#endif
