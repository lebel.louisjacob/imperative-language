#ifndef RETURN_AST_HPP
#define RETURN_AST_HPP

#include <ast/statementAst.hpp>
#include <ast/expressionAst.hpp>

#include <memory>

namespace imp
{
    struct ReturnAst final : public AstImpl<ReturnAst, StatementAst>
    {
        ReturnAst(std::unique_ptr<ExpressionAst const>&& expression):
            expression { std::move(expression) }
        {}

        std::unique_ptr<ExpressionAst const> expression;
    };
}

#endif
