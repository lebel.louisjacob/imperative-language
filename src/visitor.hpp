#ifndef VISITOR_HPP
#define VISITOR_HPP

#include <neither/either.hpp>
#include <any>
#include <utils/storable.hpp>
#include <type_traits>

namespace imp
{
    struct AnyVisitor
    {
        virtual ~AnyVisitor() = default;
    };

    template <typename _Ast>
    struct VisitorOf : public virtual AnyVisitor
    {
        virtual std::any visit_any(_Ast const& ast) const& = 0;
    };

    template <typename _Output>
    struct VisitorStaticMembers
    {
        using Output = Storable<_Output>;
        using PartialOutput = neither::Either<std::string, Output>;
    };

    template <typename _Output, typename _Ast>
    struct SingleVisitor:
        public VisitorStaticMembers<_Output>,
        public VisitorOf<_Ast>
    {
        virtual typename SingleVisitor::PartialOutput visit(_Ast const& ast) const& = 0;

    private:
        virtual std::any visit_any(_Ast const& ast) const& final override
        {
            return visit(ast);
        }
    };

    template <typename _Output, typename... _Asts>
    struct Visitor : public SingleVisitor<_Output, _Asts>...
    {};
}

#endif
