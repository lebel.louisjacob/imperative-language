#ifndef TARGET_CPP_DEFAULT_CONTROL_FLOW_STRATEGY_HPP
#define TARGET_CPP_DEFAULT_CONTROL_FLOW_STRATEGY_HPP

#include <targets/common/identifierMangler.hpp>

#include <targets/cpp/controlflowStrategy.hpp>

namespace imp
{
    struct DefaultControlflowStrategy final : ControlflowStrategy
    {
        DefaultControlflowStrategy(IdentifierMangler const& identifier_mangler):
            _identifier_mangler { identifier_mangler }
        {}

        virtual bool accepts(FunctionAst const& ast) const& final override
        {
            return true;
        }

        virtual std::string get_signature(FunctionAst const& ast) const& final override
        {
            std::string const&& parameter_name { _identifier_mangler.mangle_variable(ast.parameter_name) };
            std::string const&& name { _identifier_mangler.mangle_function(ast.name) };

            std::string const&& parameters_list { "(uint64_t " + parameter_name + ")" };
            return "uint64_t " + name + parameters_list;
        }

        virtual std::string get_statements_prefix(FunctionAst const& ast) const& final override
        {
            return "";
        }

        virtual std::string get_return_statement(std::string_view const& expression) const& final override
        {
            return "return " + std::string { expression } + ";";
        }

    private:
        IdentifierMangler const& _identifier_mangler;
    };
}

#endif
