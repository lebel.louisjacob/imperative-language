#ifndef TARGET_CPP_FUNCTION_KIND_HPP
#define TARGET_CPP_FUNCTION_KIND_HPP

#include <ast/functionAst.hpp>

#include <string_view>
#include <string>

namespace imp
{
    struct ControlflowStrategy
    {
        virtual bool accepts(FunctionAst const& ast) const& = 0;
        virtual std::string get_signature(FunctionAst const& ast) const& = 0;
        virtual std::string get_statements_prefix(FunctionAst const& ast) const& = 0;
        virtual std::string get_return_statement(std::string_view const& expression) const& = 0;
    };
}

#endif
