#ifndef TARGET_CPP_FUNCTION_FORM_HPP
#define TARGET_CPP_FUNCTION_FORM_HPP

#include <ast/functionAst.hpp>
#include <targets/cpp/controlflowStrategy.hpp>
#include <targets/common/identifierMangler.hpp>
#include <visitor.hpp>

namespace imp
{
    struct Detion
    {
        virtual VisitorStaticMembers<std::string>::PartialOutput get_function_target(
            FunctionAst const& ast,
            ControlflowStrategy const& controlflow_kind) const& = 0;
    };
}

#endif
