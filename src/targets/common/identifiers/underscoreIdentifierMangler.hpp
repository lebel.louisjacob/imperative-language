#ifndef TARGET_UNDERSCORE_IDENTIFIER_MANGLER_HPP
#define TARGET_UNDERSCORE_IDENTIFIER_MANGLER_HPP

#include <targets/common/identifierMangler.hpp>

namespace imp
{
    struct UnderscoreIdentifierMangler final : public IdentifierMangler
    {
        virtual std::string mangle_variable(std::string_view const& name) const& final override
        {
            return "_v_" + std::string { name };
        }

        virtual std::string mangle_function(std::string_view const& name) const& final override
        {
            return "_f_" + std::string { name };
        }
    } const underscore_identifier_mangler;
}

#endif
