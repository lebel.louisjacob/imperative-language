.PHONY: clean

include make/tree.mk

clean:
	rm -rf $(OUT_DIRECTORY)
