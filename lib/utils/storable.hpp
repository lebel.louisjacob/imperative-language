#ifndef UTILS_STORABLE_HPP
#define UTILS_STORABLE_HPP

#include <variant>
#include <type_traits>

namespace imp
{
    template <typename A>
    using Storable = std::conditional_t<std::is_same_v<A, void>, std::monostate, A>;
}

#endif
